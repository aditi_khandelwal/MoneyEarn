import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ExplorePage } from '../pages/explore/explore';
import { WalletPage } from '../pages/wallet/wallet';
import { JoinPage } from '../pages/join/join';
import { StartPage } from '../pages/start/start';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { NewsPage } from '../pages/news/news';
import { DeleteAccPage } from '../pages/delete-acc/delete-acc';


@NgModule({
  declarations: [
    MyApp,
    StartPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ExplorePage,
    WalletPage,
    JoinPage,
    SignupPage,
    LoginPage,
    NewsPage,
    DeleteAccPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    StartPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ExplorePage,
    WalletPage,
    JoinPage,
    SignupPage,
    LoginPage,
    NewsPage,
    DeleteAccPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
