import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';

/**
 * Generated class for the StartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }
  signup() {
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
  }
  login() {
    let modal = this.modalCtrl.create(LoginPage);
    modal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad StartPage');
  }

}
