import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteAccPage } from './delete-acc';

@NgModule({
  declarations: [
    DeleteAccPage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteAccPage),
  ],
})
export class DeleteAccPageModule {}
