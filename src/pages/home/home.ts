import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  offer: Array<{ img: string; detail: string; price: number }>;
  constructor(public navCtrl: NavController) {
    this.offer = [
      { img: "../assets/imgs/uber.png", detail: "Install and Open", price: 30 },
      { img: "../assets/imgs/uber.png", detail: "Install and Open", price: 40 },
      { img: "../assets/imgs/uber.png", detail: "Install and Open", price: 10 },
      { img: "../assets/imgs/uber.png", detail: "Install and Open", price: 20 },
      { img: "../assets/imgs/uber.png", detail: "Install and Open", price: 50 }

    ];
  }

}
