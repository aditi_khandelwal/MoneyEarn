import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { JoinPage } from '../join/join';
import { TabsPage } from '../tabs/tabs';
import { ViewController } from 'ionic-angular';
import { StartPage } from '../start/start';
import { AlertController } from 'ionic-angular';
import { DeleteAccPage } from '../delete-acc/delete-acc';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  event: boolean = true;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
  }
  ProfEdit() {
    this.navCtrl.push(JoinPage);
  }

  Signout() {
    let confirm = this.alertCtrl.create({
      title: 'SignOut',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            this.navCtrl.push(ContactPage);

          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.push(StartPage);
          }
        }
      ]
    });
    confirm.present();

  }
  DeleteAcc() {
    this.navCtrl.push(DeleteAccPage);
  }
  makeedit() {
    this.event = !this.event;
  }

}
