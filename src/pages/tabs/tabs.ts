import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ExplorePage } from '../explore/explore';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = WalletPage;
  tab2Root = HomePage;
  tab3Root = AboutPage;
  tab4Root = ExplorePage;
  tab5Root = ContactPage;

  constructor(public navCtrl: NavController) {

  }
}
