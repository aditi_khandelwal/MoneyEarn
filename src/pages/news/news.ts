import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  content: Array<Object>;
  tabBarElement: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.content = [
      { id: "1", img: "../assets/imgs/contact.jpg", title: "U.N. vote on Jerusalem Israeli", like: 290, comment: 234, date: "May 11", detail: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite thed a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit", newspaper: "Times of India" },
      { id: "2", img: "../assets/imgs/contact.jpg", title: "Lobbying intensifies for India's UNGA vote on Jerusalem", like: 11, comment: 134, date: "May 11", detail: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize JWashington 'to pay even more and to use our influence for their benefit.", newspaper: "India Now" },
      { id: "3", img: "../assets/imgs/contact.jpg", title: "A.Raja,acquitted in 2G spectrum allocation cases", like: 212, comment: 34, date: "May 11", detail: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize JerAmbassador to the UN Nikki Haley issued a direct threat, saying that the US will think and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.", newspaper: "The Hindu" },
      { id: "4", img: "../assets/imgs/contact.jpg", title: "Hung verdill alliances", like: 230, comment: 24, date: "May 11", detail: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize J nations abstained, including Canada, Mexico and Australia.The vote came after US and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.", newspaper: "Hindustan Times" },
      { id: "5", img: "../assets/imgs/contact.jpg", title: "U.N. vote on Jerusalem Israeli", like: 50, comment: 564, date: "Jan 11", detail: "The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 cou", newspaper: "The Economic Times" },
      { id: "6", img: "../assets/imgs/contact.jpg", title: "Analysts aren't Buying the sterling Rebound", like: 30, comment: 154, date: "May 12", detail: "The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 cou", newspaper: "times of india" }
    ]

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsPage');
  }

}
