import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WalletPage } from '../wallet/wallet';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { NewsPage } from '../news/news';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  news: Array<Object>;
  constructor(public navCtrl: NavController) {
    this.news = [
      { id: "1", img: "../assets/imgs/contact.jpg", title: " U.N. vote on Jerusalem Israeli", like: 290, comment: 234, date: "May 11" },
      { id: "2", img: "../assets/imgs/contact.jpg", title: "Lobbying intensifies for India's UNGA vote on Jerusalem", like: 11, comment: 134, date: "May 11" },
      { id: "3", img: "../assets/imgs/contact.jpg", title: "A.Raja,acquitted in 2G spectrum allocation cases", like: 212, comment: 34, date: "May 11" },
      { id: "4", img: "../assets/imgs/contact.jpg", title: "Hung verdill alliances", like: 230, comment: 24, date: "May 11" },
      { id: "5", img: "../assets/imgs/contact.jpg", title: "U.N. vote on Jerusalem Israeli", like: 50, comment: 564, date: "Jan 11" },
      { id: "6", img: "../assets/imgs/contact.jpg", title: "Analysts aren't Buying the sterling Rebound", like: 30, comment: 154, date: "May 12" }
    ]

  }

  newspage() {
    this.navCtrl.push(NewsPage);
  }
}
